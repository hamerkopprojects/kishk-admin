<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="float-left">
                    <div class="hamburger sidebar-toggle">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                </div>
                <div class="float-right">
                    <ul>
                        {{-- @if(Auth::guard('supplier')->check())
                        
                        @else
                        <li class="header-icon dib"><a href="{{route('notification.other')}}"><i class="ti-bell"></i>
                           @php 
                           $notificaton_count = \App\Models\AppNotifications::where('from_type', 'website')->where('admin_read', 0)->count();
                           @endphp
                           @if($notificaton_count > 0)<span class="fa fa-comment"></span>
                           <span class="num">{{$notificaton_count}}</span>@endif
                        </a>
                        </li>
                        @endif --}}
                        {{-- @if(Auth::guard('supplier')->check() || Auth::guard('branch')->check())
                        @php
                        $lang1= app()->getLocale() == 'en' ? 'ar' : 'en';
                        $display = $lang1 == 'en' ? 'English' : 'العربية';
                        @endphp
                        <li class="header-icon dib"><a href="{{ url('setlocale', $lang1) }}" class="user-avatar">{{ $display }}</a></li>
                        @endif --}}
                        <li class="header-icon dib"><span class="user-avatar">
                            {{-- @if(Auth::guard('supplier')->check() || Auth::guard('branch')->check()){{ __('messages.admin.my_account') }} @else 
                            My Account
                            @endif --}}
                            My Account
                            <i class="ti-angle-down f-s-10"></i></span>
                            <div class="drop-down dropdown-profile">
                                <div class="dropdown-content-body">
                                    <ul>
                                        {{-- @if(Auth::guard('supplier')->check())
                                        <li><a href="{{route('supplier.edit')}}"><i class="ti-user"></i> <span>{{ __('messages.admin.edit_profile') }}</span></a></li>
                                        <li><a href="{{route('supplier.logout')}}"><i class="ti-power-off"></i> <span>{{ __('messages.admin.logout') }}</span></a></li>
                                        @elseif(Auth::guard('branch')->check())
                                        <li><a href="{{route('branch.edit')}}"><i class="ti-user"></i> <span>{{ __('messages.admin.edit_profile') }}</span></a></li>
                                        <li><a href="{{route('branch.logout')}}"><i class="ti-power-off"></i> <span>{{ __('messages.admin.logout') }}</span></a></li>
                                        @else --}}
                                        <li><a href="#"><i class="ti-user"></i> <span>Edit Profile</span></a></li>
                                        <li><a href="#"><i class="ti-power-off"></i> <span>Logout</span></a></li>
                                        {{-- @endif --}}
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>