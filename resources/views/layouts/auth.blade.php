<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{config('site.name')}}</title>

    <!-- ================= Favicon ================== -->
    <link rel="icon" href="{{ asset('assets/frontend/custom/img/logo_title.png') }}" type="image/icon type">

    <!-- Styles -->
    <link href="{{ asset('assets/css/lib/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lib/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <style>
        .login-form label .error{
            color: red;
        }
        .btn{
            cursor: pointer;
        }
    </style>
    @stack('css')
</head>

<body class="bg-primary">

    <div class="unix-login">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="msg">
                        @yield('flash')
                    </div>
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="index.html"><span><img src="{{asset('assets/images/logo.png')}}"></span></a>
                        </div>

                        @yield('auth-form')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="{{asset('assets/js/jquery.validate.js')}}"></script>
    <script>
       setTimeout(function(){
        $(".alert").remove();
    }, 2000 );
    </script>
    @stack('scripts')
</body>

</html>