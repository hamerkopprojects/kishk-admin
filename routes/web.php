<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\Admin\AdminVerificationController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\RatingSegmentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Auth::routes(['register' => false]);
Route::post("login", [LoginController::class, 'login'])->name('login');
Route::post("token/send", [AdminVerificationController::class, 'resetPasswordToken'])->name('admin.verify');
Route::post("token/verify", [AdminVerificationController::class, 'tokenVerify'])->name('token.verify');
Route::post("admin/password/change", [AdminVerificationController::class, 'passwordUpdating'])->name('admin.reset');
Route::post("admin/logout", [AdminVerificationController::class, 'logout'])->name('admin.logout');

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'admin', 'middleware' => 'auth:web'], function () {
    Route::get("/dashboard", [DashboardController::class, 'get'])->name('dashboard.get');


Route::get("/segments", [RatingSegmentController::class, 'get'])->name('admin.segment.get');
Route::get("/segments/edit/{id}", [RatingSegmentController::class, 'edit'])->name('admin.segments.edit');
Route::post("/segments/update", [RatingSegmentController::class, 'update'])->name('admin.segment.update');
Route::post("/segments/status", [RatingSegmentController::class, 'statusUpdate'])->name('admin.segment.status');
});