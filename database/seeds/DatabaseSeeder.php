<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // RatingSegments::truncate();
        // RatingSegmentsLang::truncate();
        UserRoles::truncate();
        User::truncate();
        Pages::truncate();
        PagesLang::truncate();
        HowtoUse::truncate();
        HowtoUseLang::truncate();
        Slider::truncate();
        Ads::truncate();
        BusinessType::truncate();
        BusinessTypeLang::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
         $this->call(UserRoleSeeder::class);
         $this->call(UserSeeder::class);
         $this->call(SegmentSeeder::class);
         $this->call(UserAppType::class);
         $this->call(PagesTableSeeder::class);
         $this->call(HowToUseSeeder::class);
         $this->call(SliderTableSeeder::class);
          $this->call(AdvertisementsTableSeeder::class);
         $this->call(BusinessTypeSeeder::class);
    }
}
