<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersWithAdditionalData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->string('fcm_token')->nullable()->after('phone');
            $table->string('otp')->nullable()->after('phone');
            $table->enum('language',['en','ar'])->default('en')->after('phone');
            $table->dateTime('otp_generated_at')->after('phone')->nullable();
            $table->string('country_code')->after('phone')->nullable();
            $table->boolean('need_notification')->default(1)->after('phone');
            $table->unsignedBigInteger('role_id')->nullable()->after('status');
            $table->foreign('role_id')
                ->references('id')
                ->on('user_roles')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
        });
    }
}
