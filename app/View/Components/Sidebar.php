<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Facades\Auth;

class Sidebar extends Component {

    public $menuArray;
    public $adminLogoArray;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct() {
        $this->adminLogoArray = array(
            'uri' => route('dashboard.get'),
            'img' => 'assets/images/sidelogo.png'
        );
        
            //details for menu
            $this->menuArray = [
                'dashboard' => [
                    'position' => 1,
                    'level' => 1,
                    'name' => 'Dashboard',
                    'icon' => 'ti-dashboard',
                    'uri' => route('dashboard.get'),
                ],
                // 'orders' => [
                //     'position' => 2,
                //     'level' => 1,
                //     'name' => 'Orders',
                //     'icon' => 'ti-shopping-cart-full',
                //     'uri' => route('admin.order-collection'),
                // ],
                // 'products' => [
                //     'position' => 2,
                //     'level' => 2,
                //     'name' => 'Products',
                //     'icon' => 'ti-anchor',
                //     'name' => 'Products',
                //     'icon' => 'ti-shopping-cart',
                //     'submenus' => [
                //         [
                //             'name' => 'Categories',
                //             'icon' => '',
                //             'uri' => route('category'),
                //         ],
                //         [
                //             'name' => 'Brands',
                //             'icon' => '',
                //             'uri' => route('brand'),
                //         ],
                //         [
                //             'name' => 'Attributes',
                //             'icon' => '',
                //             'uri' => route('attribute'),
                //         ],
                //         [
                //             'name' => 'Products',
                //             'icon' => '',
                //             'uri' => route('products'),
                //         ],
                //        [
                //            'name' => 'Promotional Products',
                //            'icon' => '',
                //            'uri' => route('promoproduct'),
                //        ],
                //        [
                //            'name' => 'Promo Codes',
                //            'icon' => '',
                //            'uri' => route('promocode'),
                //        ],
                //     ]
                // ],
                // 'ads' => [
                //     'position' => 3,
                //     'level' => 1,
                //     'name' => 'Ads',
                //     'icon' => 'ti-gallery',
                //     'uri' => route('ads'),
                //     'inner_uri' => route('slider'),
                // ],
                // 'customers' => [
                //     'position' => 4,
                //     'level' => 1,
                //     'name' => 'Customers',
                //     'icon' => 'ti-id-badge',
                //     'uri' => route('customer'),
                // ],
                // 'users' => [
                //     'position' => 6,
                //     'level' => 1,
                //     'name' => 'Users',
                //     'icon' => 'ti-user',
                //     'uri' => route('user-list.get'),
                // ],
                // 'suppliers' => [
                //     'position' => 7,
                //     'level' => 1,
                //     'name' => 'Suppliers',
                //     'icon' => 'ti-user',
                //     'uri' => route('admin.supplier'),
                // ],
                // 'cms' => [
                //     'position' => 8,
                //     'level' => 2,
                //     'name' => 'CMS',
                //     'icon' => 'ti-book',
                //     'submenus' => [
                //         [
                //             'name' => 'FAQs',
                //             'icon' => '',
                //             'uri' => route('faq.get'),
                //         ],
                //         [
                //             'name' => 'Pages',
                //             'icon' => '',
                //             'uri' => route('pages.get'),
                //         ],
                //         [
                //             'name' => 'How to use the app',
                //             'icon' => '',
                //             'uri' => route('use.get'),
                //         ],
                //     ]
                // ],
                // 'notification' => [
                //     'position' => 9,
                //     'level' => 1,
                //     'name' => 'Notifications',
                //     'icon' => 'ti-bell',
                //     'uri' => route('notification.other'),
                // ],
                // 'support' => [
                //     'position' => 9,
                //     'level' => 1,
                //     'name' => 'Support',
                //     'icon' => 'ti-headphone',
                //     'uri' => route('support.list'),
                // ],
                'setting' => [
                    'position' => 11,
                    'level' => 2,
                    'name' => 'Settings',
                    'icon' => 'ti-anchor',
                    'submenus' => [
                        [
                            'name' => 'Rating segments',
                            'icon' => '',
                            'uri' => route('admin.segment.get'),
                        ],
                        // [
                        //     'name' => 'Cancellation reasons',
                        //     'icon' => '',
                        //     'uri' => route('cancel.get'),
                        // ],
                        // [
                        //     'name' => 'Cities',
                        //     'icon' => '',
                        //     'uri' => route('city.get'),
                        // ],
                        // [
                        //     'name' => 'Regions',
                        //     'icon' => '',
                        //     'uri' => route('region.get'),
                        // ],
                        // [
                        //     'name' => 'Commission categories',
                        //     'icon' => '',
                        //     'uri' => route('commision.get'),
                        // ],
                        // [
                        //     'name' => 'Other settings',
                        //     'icon' => '',
                        //     'uri' => route('other.get')
                        // ],
                    ],
                ],
            ];
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render() {
        return view('components.sidebar');
    }

}
