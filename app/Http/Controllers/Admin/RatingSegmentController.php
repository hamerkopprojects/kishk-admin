<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\RatingSegments;
use App\Models\RatingSegmentsLang;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class RatingSegmentController extends Controller
{
    public function get()
    {
        $rating=RatingSegments::with('lang')->get();
        return view('admin.settings.rating.index',compact('rating'));
    }

    public function edit($id)
    {
        $rating=RatingSegments::with('lang')->where('id',$id)->first();
        return[
            'rating' => $rating
        ];
    }
    public function update(Request $request)
    {
        DB::transaction(function () use ($request) {
            RatingSegmentsLang::where('language','en')
            ->where('rating_segment_id',$request->id)
            ->update([
                'name'=>$request->segmentTitleEn
            ]);
            RatingSegmentsLang::where('language','ar')
            ->where('rating_segment_id',$request->id)
            ->update([
                'name'=>$request->segmentTitleAr
            ]);
        });

        return [
            'status'=>'200',
            'msg'=>'Success'
        ];
    }
    public function statusUpdate(Request $req)
    {
        $status=$req->status === 'Deactivate' ? 'deactive':'active';
        RatingSegments::where('id',$req->id)->update([
            'status'=>$status
        ]);

        return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
    }
}
